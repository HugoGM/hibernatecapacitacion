package mx.edu.uacm.hibernate.DAO.Impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import mx.edu.uacm.hibernate.DAO.OperacionesProductos;
import mx.edu.uacm.hibernate.model.HibernateUtility;
import mx.edu.uacm.hibernate.model.Producto;

public class OperacionesProductoImpl implements OperacionesProductos {
	
	private HibernateUtility hbUtility;
	private SessionFactory sF;
	
	public OperacionesProductoImpl() {
		hbUtility = new HibernateUtility();
		sF = hbUtility.getSessionFactory();
	}

	public Producto findById(int id) {
		Producto producto = null;
		Session sesion = sF.openSession();
		NativeQuery query = sesion.createNativeQuery("SELECT id, descripcion, nombre, precio, carrito_id FROM producto WHERE id = ?", Producto.class);
		query.setParameter(1, id);
		producto = (Producto) query.getSingleResult();		
		return producto;
	}

	public List<Producto> findAll() {
		List<Producto> productos = null;
		Session sesion = sF.openSession();
		
		sesion.getTransaction().begin();
		NativeQuery query = sesion.createNativeQuery("SELECT id, descripcion, nombre, precio, carrito_id FROM producto", Producto.class);
		productos = query.getResultList();
		sesion.getTransaction().commit();
		
		return productos;
	}

	public List<Producto> executeQuery(String q) {
		List<Producto> productos = null;
		Session sesion = sF.openSession();
		
		NativeQuery nativeQuery = sesion.createNativeQuery(q);
		productos = nativeQuery.getResultList();
		
		
		return productos;
		
	}

	public List<Producto> hQuery(String q) {
		List<Producto> productos = null;
		Session sesion = sF.openSession();
		Query<Producto> query = sesion.createQuery(q,  Producto.class);
		productos = query.list();
		return productos;
	}
	
}
