package mx.edu.uacm.hibernate.DAO.Impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import mx.edu.uacm.hibernate.DAO.OperacionesCarrito;
import mx.edu.uacm.hibernate.model.Carrito;
import mx.edu.uacm.hibernate.model.HibernateUtility;
import mx.edu.uacm.hibernate.model.Producto;

public class OperacionesCarritoImpl implements OperacionesCarrito {
	
	private HibernateUtility hbUtility;
	private SessionFactory sF;
	
	public OperacionesCarritoImpl() {
		hbUtility = new HibernateUtility();
		sF = hbUtility.getSessionFactory();
		
	}

	public Carrito findById(int id) {
		Carrito carrito = null;
		Session sesion = sF.openSession();
		
		sesion.getTransaction().begin();
		NativeQuery query = sesion.createNativeQuery("SELECT id FROM carrito WHERE id = ?", Carrito.class);
		query.setParameter(1, id);
		carrito = (Carrito) query.getSingleResult();
		sesion.getTransaction().commit();
		
		return carrito;
	}

	public List<Carrito> findAll() {
		List<Carrito> carritos = null;
		Session sesion = sF.openSession();
		
		sesion.getTransaction().begin();
		NativeQuery<Carrito> query = sesion.createNativeQuery("SELECT id, descripcion, nombre, precio, carrito_id FROM productos");
		carritos = query.getResultList();
		sesion.getTransaction().commit();
		
		return carritos;
	}

	public List<Carrito> executeQuery(String q) {
		List<Carrito> carritos = null;
		Session sesion = sF.openSession();
		
		NativeQuery<Carrito> nativeQuery = sesion.createNativeQuery(q);
		carritos = nativeQuery.getResultList();
		
		return carritos;
	}

	public List<Carrito> hQuery(String q) {
		List<Carrito> carritos = null;
		Session sesion = sF.openSession();
		Query<Carrito> query = sesion.createQuery(q);
		carritos = query.list();
		return carritos;
	}

}
