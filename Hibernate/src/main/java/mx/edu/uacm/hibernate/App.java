package mx.edu.uacm.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import mx.edu.uacm.hibernate.DAO.OperacionesCarrito;
import mx.edu.uacm.hibernate.DAO.OperacionesProductos;
import mx.edu.uacm.hibernate.DAO.Impl.OperacionesCarritoImpl;
import mx.edu.uacm.hibernate.DAO.Impl.OperacionesProductoImpl;
import mx.edu.uacm.hibernate.model.Carrito;
import mx.edu.uacm.hibernate.model.HibernateUtility;
import mx.edu.uacm.hibernate.model.Producto;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        HibernateUtility hbUtility = new HibernateUtility();
        
        SessionFactory sF = hbUtility.getSessionFactory();
        Session sesion = sF.openSession();
        
        List<Producto> prods = null;
        
        
        OperacionesProductos operaciones = new OperacionesProductoImpl();
        OperacionesCarrito carritos = new OperacionesCarritoImpl();
        
        //Guardando un carrito con sus productos en la base de datos
        /*
         Carrito carrito = new Carrito();
        ArrayList<Producto> productos = new ArrayList();
        
        Producto producto = new Producto();
        producto.setNombre("Zukaritas");
        producto.setDescripcion("Cereal con azucar");
        producto.setPrecio(34.50);
        
        productos.add(producto);
        carrito.setProductos(productos);
        producto.setCarrito(carrito);      
        
        sesion.getTransaction().begin();
        sesion.save(carrito);
        sesion.getTransaction().commit();
         */
        
        
        //Buscando un producto por su id
        /*
        Producto prod = operaciones.findById(1);
        
        System.out.println(prod.toString());*/
        
        //Obteniendo todos los productos e imprimiendo la cantidad
        /*
         prods = operaciones.findAll();
         System.out.println(prods.size());
         */
        
        //Buscando un carrito por id
        /*     
        Carrito carr = carritos.findById(1);
        
        System.out.println(carr.getId());*/
        
        //HQL obteniendo el producto con id 3
       /* prods = operaciones.hQuery("from producto where id=3");
        System.out.println(prods.size());*/
        
        //Número de productos que contiene el carrito con id 1 
        /*
        Query q = sesion.createQuery("select count(id) from producto where carrito_id = 1");
        System.out.println((Long) q.getSingleResult());*/
        
        //Total del precio de productos del carrito 
        /*
        Query q = sesion.createQuery("select sum(precio) from producto where carrito_id = 1");
        System.out.println((Double) q.getSingleResult());*/
        
        //Buscando un producto dada una expresión
        
        Query q = sesion.createQuery("from producto where nombre like 'Ch%'", Producto.class);
        Producto p = (Producto) q.getSingleResult();
        System.out.println((p.getCarrito().getId() + "," + p.getId()  + "," + p.getNombre()) );
        
        sesion.close();
        
        
        
        
    }
}
