package mx.edu.uacm.hibernate.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.NativeQuery;

import mx.edu.uacm.hibernate.model.Carrito;

public interface OperacionesCarrito {
	
	Carrito findById(int id);
	List<Carrito> findAll();
	List<Carrito> executeQuery(String q);
	List<Carrito> hQuery(String q);

}
