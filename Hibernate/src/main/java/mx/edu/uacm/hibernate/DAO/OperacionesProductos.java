package mx.edu.uacm.hibernate.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.NativeQuery;

import mx.edu.uacm.hibernate.model.Carrito;
import mx.edu.uacm.hibernate.model.Producto;

public interface OperacionesProductos {
	
	Producto findById(int id);
	List<Producto> findAll();
	List<Producto> executeQuery(String q);
	List<Producto> hQuery(String q);
	
	
}
