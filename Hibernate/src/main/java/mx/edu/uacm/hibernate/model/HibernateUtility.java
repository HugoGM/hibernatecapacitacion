package mx.edu.uacm.hibernate.model;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtility {
	
	private StandardServiceRegistry sR;
	private SessionFactory sF;
	
	public HibernateUtility() {
		
	}
	
	public SessionFactory getSessionFactory() {
		sR = new StandardServiceRegistryBuilder().configure().build();
		
		sF = new MetadataSources(sR).buildMetadata().buildSessionFactory();
		
		return sF;
	}

}
